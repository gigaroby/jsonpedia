package com.machinelinking.enricher;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Test case for {@link WikiEnricherFactory}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class WikiEnricherFactoryTest {

    @Test
    public void testToFlagsJustDefault() {
        final Flag[] DEFAULTS = new Flag[]{ WikiEnricherFactory.Structure, WikiEnricherFactory.Validate };
        Assert.assertEquals(
                Arrays.asList(DEFAULTS),
                Arrays.asList(WikiEnricherFactory.getInstance().toFlags("", DEFAULTS))
        );
    }

    @Test
    public void testToFlagsOptionalAdded() {
        final Flag[] DEFAULTS = new Flag[]{ WikiEnricherFactory.Structure, WikiEnricherFactory.Validate };
        Assert.assertEquals(
                Arrays.asList( new Flag[]{
                        WikiEnricherFactory.Structure, WikiEnricherFactory.Validate, WikiEnricherFactory.Linkers
                }),
                Arrays.asList(WikiEnricherFactory.getInstance().toFlags("Linkers", DEFAULTS))
        );
    }

    @Test
    public void testToFlagsDefaultRemoved() {
        final Flag[] DEFAULTS = new Flag[]{ WikiEnricherFactory.Structure, WikiEnricherFactory.Validate };
        Assert.assertEquals(
                Arrays.asList( new Flag[]{ WikiEnricherFactory.Structure }),
                Arrays.asList(WikiEnricherFactory.getInstance().toFlags("-Validate", DEFAULTS))
        );
    }

    @Test
    public void testToFlagsOptionalAddedDefaultRemoved() {
        final Flag[] DEFAULTS = new Flag[]{ WikiEnricherFactory.Structure, WikiEnricherFactory.Validate };
        Assert.assertEquals(
                Arrays.asList( new Flag[]{ WikiEnricherFactory.Validate, WikiEnricherFactory.Linkers } ),
                Arrays.asList(WikiEnricherFactory.getInstance().toFlags("Linkers,-Structure", DEFAULTS))
        );
    }

}
