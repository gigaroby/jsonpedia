package com.machinelinking.service;

/**
 * Service marker interface.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface Service {
}
