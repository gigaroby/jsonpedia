/**
 * Conversion of parser events ({@link com.machinelinking.parser.WikiTextParserHandler})
 * to serialization events ({@link com.machinelinking.serializer.Serializer}).
 */
package com.machinelinking.pagestruct;
