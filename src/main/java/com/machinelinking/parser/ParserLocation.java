package com.machinelinking.parser;

/**
 * Defines the location of the parser cursor.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface ParserLocation {

    int getRow();

    int getCol();

}
