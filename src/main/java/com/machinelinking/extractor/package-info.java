/** Core {@link com.machinelinking.extractor.Extractor}s.
 * An {@link com.machinelinking.extractor.Extractor} allows to collect specific data from the page event stream. */
package com.machinelinking.extractor;
