package com.machinelinking.storage;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class MultiJSONStorage implements JSONStorage<MultiJSONStorageConfiguration, MultiDocument, MultiSelector> {

    private final MultiJSONStorageConfiguration configuration;
    private final DocumentConverter<MultiDocument> converter;

    private final JSONStorage[] internalStorages;

    protected MultiJSONStorage(
            MultiJSONStorageConfiguration configuration,
            DocumentConverter<MultiDocument> converter,
            JSONStorage[] internalStorages
    ) {
        this.configuration = configuration;
        this.converter = converter;
        this.internalStorages = internalStorages;
    }

    @Override
    public MultiJSONStorageConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public DocumentConverter<MultiDocument> getConverter() {
        return converter;
    }

    @Override
    public boolean exists() {
        return exists(null);
    }

    @Override
    public boolean exists(String collection) {
        final String targetConnection = collection == null ? configuration.getCollection() : collection;
        boolean exist, notexist; exist = notexist = false;
        for (JSONStorage storage : internalStorages) {
            if(storage.exists(targetConnection))
                exist = true;
            else
                notexist = true;
        }
        if(exist && notexist)throw new IllegalStateException();
        return exist;
    }

    @Override
    public JSONStorageConnection<MultiDocument, MultiSelector> openConnection() {
        return openConnection(null);
    }

    @Override
    public MultiJSONStorageConnection openConnection(String collection) {
        final String targetCollection = collection == null ? configuration.getCollection() : collection;
        final List<JSONStorageConnection> connections = new ArrayList<>();
        for(JSONStorage storage : internalStorages) {
            connections.add(storage.openConnection(targetCollection));
        }
        return new MultiJSONStorageConnection(connections.toArray(new JSONStorageConnection[connections.size()]));
    }

    @Override
    public void deleteCollection() {
        deleteCollection(null);
    }

    @Override
    public void deleteCollection(String collection) {
        final String targetConnection = collection == null ? configuration.getCollection() : collection;
        for(JSONStorage storage : internalStorages) {
            storage.deleteCollection(targetConnection);
        }
    }

    @Override
    public void close() {
        for(JSONStorage storage : internalStorages) {
            storage.close();
        }
    }

}
