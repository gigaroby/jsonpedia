package com.machinelinking.storage;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class SelectorParserException extends Exception {

    public SelectorParserException(String message) {
        super(message);
    }

    public SelectorParserException(String message, Throwable cause) {
        super(message, cause);
    }

}
