/**
 * Support to create <i>Elasticsearch</i> indexes specific for faceting purposes.
 */
package com.machinelinking.storage.elasticsearch.faceting;