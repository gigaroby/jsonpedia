package com.machinelinking.storage;

/**
 * Defines a <i>JSON</i> storage configuration.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface JSONStorageConfiguration {

    String getDB();

    String getCollection();

}
