package com.machinelinking.render;

/**
 * Factory for an {@link HTMLRender}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface HTMLRenderFactory {

    HTMLRender createRender();

}
