package com.machinelinking.render;

/**
 * Root render interface.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface RootRender extends NodeRender, KeyValueRender {

}
