package com.machinelinking.render;

import org.codehaus.jackson.JsonNode;

import java.io.IOException;

/**
 * Defines the renderer for a primitive {@link JsonNode} value.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface PrimitiveNodeRender {

    boolean render(JsonContext context, JsonNode node, HTMLWriter writer) throws IOException;

}
