/**
 * Support for splitting hierarchical event streams into event sub trees.
 * Redirects events of a certain depth from a
 * {@link com.machinelinking.parser.WikiTextParserHandler} to another one.
 */
package com.machinelinking.splitter;