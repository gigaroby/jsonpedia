/**
 * Provides support for <i>Wikimedia Template</i> processing.
 *
 * @author Michele Mostarda (mostarda@fbk.eu) 
 */
package com.machinelinking.template;