package com.machinelinking.template;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class TemplateCallHandlerException extends Exception {

    public TemplateCallHandlerException(String message) {
        super(message);
    }

    public TemplateCallHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

}
