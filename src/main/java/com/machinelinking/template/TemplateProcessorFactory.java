package com.machinelinking.template;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface TemplateProcessorFactory {

    TemplateProcessor createProcessor();

}
